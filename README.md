# My magic prompt

simulation of some terminal commands

## Installation

 - chmod +x main.sh 
 - ./main.sh

## commands

 - ls : list files and folders visible as hidden
 - rm : delete a file
 - rmd ou rmdir : delete a folder
 - about :  program description
 - version ou --v ou vers :  displays the version of the prompt
 - age : asks your age and tells you if you are an adult or a minor
 - quit : exit
 - profile : allows you to display all the information .
 - passw :  allows you to change the password
 - cd : go to a folder you just created or go back to a previous
 - pwd : indicates the current directory 
 - hour :  give the present time
 - * : indicate an unknown order
 - httpget :  allows you to download the html source code of a web page and save it in a file of type
 - smtp: send mail

## License
[MIT](https://choosealicense.com/licenses/mit/)
