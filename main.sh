#!/bin/sh
password="admin"

age()
{
    echo -n "votre age : "
    read age
    if [ $((age)) -ge 18 ];then
        echo "vous êtes majeur"
    else
        echo "vous êtes mineur"
    fi
}

version()
{
    echo "Version 1.0"
}

profil()
{
    echo "Nom: Doisy"
    echo "prenom: Elton"
    echo "email: elton.doisy@gmail.com"
}

about()
{
	echo "simulation de certaines commandes de terminal"
}

help()
{
    echo "ls : lister des fichiers et les dossiers visible comme caché"
    echo "rm : supprimer un fichier"
    echo "rmd ou rmdir : supprimer un dossier"
    echo "about : une description de votre programme"
    echo "version ou --v ou vers :  affiche la version de votre prompt"
    echo "age : vous demande votre âge et vous dit si vous êtes majeur ou mineur"
    echo "quit : permet de sortir du prompt"
    echo "profile : permet d’afficher toutes les informations sur vous même."
    echo "passw : permet de changer le password avec une demande de confirmation"
    echo "cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier précédent"
    echo "pwd : indique le répertoire actuelle courant"
    echo "hour : permet de donner l’heure actuelle"
    echo "* : indiquer une commande inconnu"
    echo "httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier"
}

passw()
{
    securePass
    echo -n "saisissez votre nouveau mot de passe : "
    read -s mdp

    echo -n "confirmez votre nouveau mot de passe : "
    read -s confMdp

    if [ "$confMdp" != "$mdp" ];then
        echo "mot de passe pas identique"
    else
        echo "mot de passe modifier!"
        password="$mdp"
    fi

}

remove()
{
  rm "$arg"
}

removeDo()
{
  rm -r "$arg"
}

secureLog()
{
    echo -n "Login : "
    read login
    while [ "$login" != "admin" ]
    do
        echo -n "Mauvais login,veuillez renseigner le bon login : "
        read login
    done
}

securePass()
{
    echo -n "Mot de passe : "
    read -s mdp

    while [ "$mdp" != "$password" ]
    do
        echo -n "Mauvais mot de passe,veuillez renseigner le bon mot de passe : "
        read mdp
    done
    echo -e "\n"
}

httpget()
{
    echo -n "Site URL : "
    read url
    echo -n "nom du fichier : "
    read filename
    wget -nv -O $filename $url
    echo "$url est enregistrer dans $filename"
}

cdDir()
{
    echo -n "Sélectionnez le répertoire: "
    read directory
    if [ -z $directory ]; then
        cdDir
    fi   
    mkdir -p $directory
    cd $directory    
}

cmd()
{
    echo -n "action : "
    read action arg
    while [ "$action" != "quit" ]
    do
        case "$action" in

            "version" |  "vers" | "--v") version;;
            "age") age;;
            "profil") profil;;
            "ls") ls -a;;
            "cd") cdDir;;
            "hour") date +%T;;
            "pwd") pwd;;
            "passw") passw;;
            "help") help;;
            "rm") remove $arg;;
            "rmd" | "rmdir") removeDo $arg;;
			"httpget") httpget;;
            "*") echo "commande inconnu";;
        esac

        echo -n "action : "
        read action

    done
}

main()
{
    secureLog
    securePass
    cmd
}

main
